# Linux操作系统
#### 课程简介
* 授课教师：丁利华
* 教师邮箱：aliceding19900924@gmail.com
* 先导课程：计算机组成原理（[北京大学陆俊林老师](https://www.coursera.org/learn/jisuanji-zucheng?#syllabus)）、操作系统([台湾清华大学黄能富老师](https://www.coursera.org/learn/jisuanji-zucheng?#syllabus))、数据结构、汇编语言、C语言或其他一门高级语言
* 课程内容与基本要求：通过本课程的学习，结合对LINUX操作系统的详尽分析，要求学会操作系统主要功能模块甚至整体系统的设计、开发技术，并能熟练运用。细分为五个目标：
  1. 能弄懂操作系统各模块的基本原理，并弄懂各模块之间的关系。
  2. 能够在操作系统原理和实现之间自由切换思维，能读懂操作系统源码，能看到源代码背后的操作系统原理。
  3. 能够对规模较小的实验用操作系统上进行全面控制，建立初步的系统实践能力。
  4. 能够在实验用小操作系统上设计实现一些规模较大的完整模块，开始掌控整个计算机系统。
  5. 能够在头脑中将整个操作系统立起来，能为单片机等小型设备配置小型操作系统，为大型操作系统上的实践积累基础
* 教学内容包括：操作系统原理综述；LINUX内核学习（进程、虚存管理、文件系统、系统启动流程、LINUX内核分析技巧、嵌入式系统开发环境）；LINUX模块分析、设计、实现。
* 课程实验请参考 [Linux操作系统实验](https://github.com/wangzhike/hit-linux-0.11)
* linux源代码请参考 [Linux内核源代码](https://kernel.org/)
* [查询Linux命令](https://www.lzltool.com/LinuxCommand)
* linux0.11源代码在线阅读请参考 [linux0.11源代码在线阅读](https://elixir.bootlin.com/linux/0.11/source)
* C语言编译汇编语言在线工具[C语言编译汇编语言在线工具](https://godbolt.org/)
* C函数调用过程栈帧分析[C函数调用过程栈帧分析](https://www.cnblogs.com/sddai/p/9762968.html)
* 数据库管理系统实现基础讲义[数据库管理系统实现基础讲义](https://oceanbase-partner.github.io/lectures-on-dbms-implementation/)
* CPU内存访问要求对齐的原因[CPU内存访问要求对齐的原因](https://blog.csdn.net/hehe199807/article/details/109091843)
* 全国大学生计算机系统能力大赛[全国大学生计算机系统能力大赛](https://os.educg.net/#/)

##### 课程对应学科竞赛

* 操作系统功能赛
* 操作内核赛
* OceanBase数据开发大赛
* 数据库管理系统设计赛


##### 期末考核课程设计
* 可任选下列课程设计题目之一完成，完成标准：需通过git完成实验代码，gitlab number通过共享文档提交，课程设计提交仓库地址为：（https://gitlab.com/Alice0924/linux-course-project） 每位同学新建分支提交代码，分支名为姓名全拼
1. 鼠标驱动和简单的图形接口实现（可参考《操作系统原理实现、设计与实践》中第十二章）
2. proc文件系统的实现（可参考《操作系统原理实现、设计与实践》中第九章）